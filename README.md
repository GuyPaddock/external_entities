# External entities

Allows using remote entities, for example through a REST interface.

## Installation

This module requires composer for installation. To install, simply run
``composer require drupal/external_entities``.
